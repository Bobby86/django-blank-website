from django.conf.urls import url
from django.contrib.auth import views as auth_views

from .forms import UserLoginForm
from .views import signup_page

app_name = 'authapp'
urlpatterns = [
    url(r'^login/$', auth_views.login, 
        {
            'template_name': 'authapp/login.html',
            'authentication_form': UserLoginForm
        }, 
        name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^signup/$', signup_page, name='signup'),
]