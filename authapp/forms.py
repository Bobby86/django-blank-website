import re

from django import forms
from django.forms import ModelForm, CharField, TextInput, PasswordInput, ValidationError
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


class UserLoginForm(AuthenticationForm):
    username = forms.CharField(max_length=254, label='', widget=TextInput(attrs = {'placeholder': 'login'}))
    password = forms.CharField(max_length=254, label='', widget=PasswordInput(attrs = {'placeholder': 'password'}))


class UserSignupForm(ModelForm):
    confirm_password = CharField(max_length=16, label='', widget=PasswordInput(attrs = {'placeholder': 'confirm password'}))

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'confirm_password']
        labels = {
            'email': '', 
            'username': '', 
            'password': '', 
        }
        widgets = {
            'email' : TextInput(attrs = {'placeholder': 'e-mail'}),
            'username' : TextInput(attrs = {'placeholder': 'login'}),
            'password' : PasswordInput(attrs = {'placeholder': 'password'}),            
        }

    def __init__(self, *args, **kwargs):
        super(UserSignupForm, self).__init__(*args, **kwargs)

        for fieldname in self.Meta.fields:
            self.fields[fieldname].help_text = None

    def clean(self):
        cleaned_data = super(ModelForm, self).clean()

        # sanitization of password
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password and password != confirm_password:
            error_message = "password and confirm password fields does not match"
            self.add_error('password', error_message)

        # sanitization of username
        login = cleaned_data.get("username")

        if not re.search(r'^[a-zA-Z]\w{3,14}$', login):
            error_message = "Login must contain only alfanumeric characters and the underscore"
            self.add_error('username', error_message)
            return
        else:
            try:
                User.objects.get(username=login)
            except ObjectDoesNotExist:
                return
            else:
                error_message = "The same user login does already exist"
                self.add_error('username', error_message)