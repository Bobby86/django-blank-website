from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.views.decorators.http import require_GET

@require_GET
def home_view(request):
    return render(request, 'homeapp/home.html', {
        'content': "Hey, folks! Here you are able to register and login, also check my ",
        'goappLinkText': "Go web site",
        })

def gotogo(request):
    return redirect('https://self-praiser-app.herokuapp.com/')

