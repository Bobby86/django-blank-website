from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .forms import UserSignupForm


def signup_page(request):
    if request.method == 'POST':
        form = UserSignupForm(request.POST)

        if form.is_valid():
            user = User.objects.create_user(
                username = form.cleaned_data['username'],
                email = form.cleaned_data['email'],
                password = form.cleaned_data['password']
            )
            return HttpResponseRedirect(reverse('authapp:login'))
        # form is not valid
        return render(request, 'authapp/signup.html', {'form': form})
    #GET
    form = UserSignupForm()
    return render(request, 'authapp/signup.html', {'form': form})
