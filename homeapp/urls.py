from django.conf.urls import url
from django.shortcuts import redirect

from . import views


app_name = 'homeapp'
urlpatterns = [
    url(r'^goapp/', views.gotogo, name='goapp'),
    url(r'', views.home_view, name='home'),    
]